const path = require("path")

const settings = require("./settings.js")

// const MiniCssExtractPlugin = require("mini-css-extract-plugin")

module.exports = {
  mode: settings.env,
  entry: [
    "./lib/client/index.js"
  ],
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "static", "public", "js")
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: "babel-loader",
        options: { presets: ["@babel/env"] }
      },
      {
        test: /\.js$/,
        include: path.resolve(__dirname, "src"),
        loader: "babel-loader"
      },
      {
        test: /\.css$/,
        use:["style-loader", "css-loader"]
      },
      {
        test: /\.(eot|woff|woff2|svg|ttf)([\?]?.*)$/,
        loader: "file-loader",
      },
      {
        test: /\.png$/,
        use: [
          'file-loader'
        ]
      },
      {
        test: /\.svg$/,
        loader: 'svg-inline-loader'
      }
    ]
  },
  // plugins: [
  //   new MiniCssExtractPlugin({
  //     filename:"bundle.css"
  //   })
  // ],
  watchOptions: {
    ignored: /node_modules/
  },
}
