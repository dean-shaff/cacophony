const path = require("path")

require("dotenv").config()


module.exports = {
  port: process.env.PORT || 8000,
  env: process.env.NODE_ENV || "development",
  // Environment-dependent settings
  development: {
    db: {
      dialect: "sqlite",
      storage: "db/db.sqlite",
      logging: false
    },
    media: {
      storage: "local",
      dir: path.join(__dirname, "media")
    }
  },
  test: {
    db: {
      dialect: "sqlite",
      storage: ":memory:",
      logging: false
    },
    media: {
      storage: "local",
      dir: path.join(__dirname, "media")
    }
  },
  production: {
    db: {
      url: process.env.DATABASE_URL,
      dialect: "postgres",
    },
    media: {
      storage: "s3"
    }
  }
};
