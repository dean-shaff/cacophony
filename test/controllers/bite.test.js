"use strict"
const path = require("path")
const fs = require("fs")

const { expect } = require('@hapi/code');
const { init } = require("./../../lib/server.js")

const controllers = require("./../../lib/controllers")

const testFilePath = path.join(__dirname, "test.mp3")


describe("bite", () => {

  let server
  let newUser
  let newBite

  beforeEach(async () => {
    server = await init();
    newUser = await controllers.user.create({
      payload: {
        name: "me",
        email: "me@address.com"
      }
    })
    let id = newUser.dataValues.id
    let buffer = fs.readFileSync(testFilePath)
    newBite = await controllers.bite.create({
      payload: {
        "title": "test.mp3",
        "description": "description for test.mp3",
        "id": id,
        "buffer": buffer
      }
    })
  });

  afterEach(async () => {
    await server.stop();
    await controllers.bite.delete({
      params: {
        "id": newBite.dataValues.id
      }
    })
  });


  test("POST /bite", async () => {
    const buffer = fs.readFileSync(testFilePath)
    let id = newUser.dataValues.id
    const res = await server.inject({
      method: "POST",
      url: "/bite",
      payload: {
        "title": "other title",
        "description": "other description",
        "id": id,
        "buffer": buffer
      }
    })
    expect(res.statusCode).to.equal(200)
    expect(res.result.title).to.equal("other title")
    await controllers.bite.delete({
      params: {
        "id": res.result.dataValues.id
      }
    })
  })
  test("GET /bites", async () => {
    const res = await server.inject({
      method: "GET",
      url: "/bites"
    })
    expect(res.statusCode).to.equal(200);
  })

  test("GET /bite/{id}", async () => {
    const res = await server.inject({
      method: "GET",
      url: `/bite/${newBite.dataValues.id}`,
    })
    expect(res.statusCode).to.equal(200)
    expect(res.result.title).to.equal("test.mp3")
  })

  test("PUT /bite/{id}", async () => {
    const res = await server.inject({
      method: "PUT",
      url: `/bite/${newBite.dataValues.id}`,
      payload: {
        title: "new title"
      }
    })
    expect(res.statusCode).to.equal(200)
    expect(res.result.title).to.equal("new title")
  })

  test("DELETE /bite/{id}", async () => {
    const res = await server.inject({
      method: "DELETE",
      url: `/bite/${newBite.dataValues.id}`
    })
    expect(res.statusCode).to.equal(204)
  })

  test("GET /media/{key}", async () => {
    const mediakey = newBite.dataValues.mediakey
    console.log(`mediakey=${mediakey}`)
    const res = await server.inject({
      method: 'GET',
      url: `/media/${mediakey}`
    })
    expect(res.statusCode).to.equal(200)
    console.log(res.result)
  })

})
