// populate the dev sqlite3 database with some fake data
const path = require("path")
const fs = require("fs")

const { init } = require("./../lib/server.js")
const controllers = require("./../lib/controllers")

const testDataDir = path.join(path.dirname(__dirname), "test/test_data")

const users = [
  {
    name: "Dean Shaff",
    email: "dean.shaff@gmail.com"
  },
  {
    name: "Will Wheaton",
    email: "will.wheaton@gmail.com"
  }
]


const main = async () => {
  let server = await init()

  // create some users
  let userObjs = await Promise.all(users.map((user)=>{
    return controllers.user.create({
      payload: user
    })
  }))
  let testDataFileNames = fs.readdirSync(testDataDir)

  let bitesPerUser = Math.floor(testDataFileNames.length / userObjs.length)
  let bites = []
  for (let idx=0; idx<userObjs.length; idx++) {
    for (let idy=0; idy<bitesPerUser; idy++) {
      let biteMediaIdx = idy + idx*bitesPerUser
      let user = userObjs[idx].dataValues
      let fileName = testDataFileNames[biteMediaIdx]
      let filePath = path.join(testDataDir, fileName)
      let buffer = fs.readFileSync(filePath)

      let bite = await controllers.bite.create({
        payload: {
          "title": `Title for ${fileName}`,
          "description": `Description for ${fileName}`,
          "userID": user.id,
          "buffer": buffer
        }
      })
      bites.push(bite)
    }
  }
  console.log(bites)


  await server.stop();
}

main()
