"use strict";

const models = require("../models/");

module.exports = async (request, h) => {
  console.log(`controllers/home.js`)
  const result = await models.Bite.findAll({
    order: [["date", "DESC"]]
  });
  console.log(result)
  // console.log(`controllers/home.js: result=${result}`)
  // let resultSim = []
  // for (let idx=0; idx<10; idx++) {
  //   resultSim.push({
  //     "title": `Hello from ${idx}`,
  //     "date": "Today"
  //   })
  // }
  // console.log(`controllers/home.js: resultSim.length=${resultSim.length}`)

  return h.view("home", {
    page: "Cacophony",
    description: "View Others' Sound Bites!",
    data: {
      title: "Cacophony",
      bites: result
    }
  })
};
