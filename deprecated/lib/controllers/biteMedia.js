const uuid = require("uuid")

const { BiteMedia } = require("./../models")

module.exports = {
  "create": async (request, h) => {
    let buffer = request.payload.buffer
    if (buffer.hasOwnProperty("data")) {
      buffer = Buffer.from(buffer.data)
    }
    const key = request.payload.key
    console.log(`controllers/biteMedia.js create: key=${key}`)
    const result = await BiteMedia.create({
      "key": key,
      "id": uuid.v4()
    })
    await result.setBuffer(buffer)
    return result
  },
  "get": async (request, h) => {
    const result = await BiteMedia.findOne({
      where: {
        id: request.params.id
      }
    })
    await result.getBuffer()
    return result
  },
  "update": async (request, h) => {},
  "delete": async (request, h) => {}
}
