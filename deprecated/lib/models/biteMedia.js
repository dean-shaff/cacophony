"use strict";

const uuid = require("uuid")
const Moment = require("moment");
const AWS = require("aws-sdk");

const accessKeyId = process.env["AWS_ACCESS_KEY_ID"]
const secretAccessKey = process.env["AWS_SECRET_KEY"]

if (accessKeyId === "" || secretAccessKey === "") {
  console.error("controllers/biteMedia.js: AWS tokens are not set")
}

const s3 = new AWS.S3({
  "accessKeyId": accessKeyId,
  "secretAccessKey": secretAccessKey
});

const bucketName = "cacophony"

const loadMedia = async function (mediaKey) {
  console.log(`models/biteMedia.js: loadMedia(${mediaKey})`)
  const params = {Bucket: bucketName, Key: mediaKey}
  return new Promise((resolve, reject) => {
    s3.getObject(params, (err, data) => {
      if (err) {
        reject(err)
      } else {
        resolve(data)
      }
    })
  })
}

const saveMedia = async function (mediaKey, mediaObject) {
  console.log(`models/biteMedia.js: saveMedia(${mediaKey})`)
  const params = {Bucket: bucketName, Key: mediaKey, Body: mediaObject};
  return new Promise((resolve, reject) => {
    s3.putObject(params, (err, data) => {
      if (err) {
        reject(err)
      } else {
        resolve(data)
      }
    })
  })
}


module.exports = (sequelize, DataTypes) => {
  const BiteMedia = sequelize.define("BiteMedia", {
    key: DataTypes.STRING,
    buffer: DataTypes.VIRTUAL,
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: uuid.v4()
    },
  });
  BiteMedia.prototype.getBuffer = async function () {
    this.buffer = await loadMedia(this.key)
    return this.buffer
  }
  BiteMedia.prototype.setBuffer = async function (buffer) {
    this.buffer = buffer
    return saveMedia(this.key, this.buffer)
  }
  return BiteMedia;
};
