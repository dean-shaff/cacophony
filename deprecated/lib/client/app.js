import play from '@fortawesome/fontawesome-free/svgs/regular/play-circle.svg'


const updateBite = function (evt) {
  console.log(`updateBite: ${evt}`)
}

const deleteBite = function (evt) {
  console.log(`deleteBite: ${evt}`)
}

const loadBite = function (evt, mediakey) {
  console.log(play)
  let src = `media/${mediakey}`
  let button = evt.target
  let toggle = button.getAttribute('toggle')
  let div = button.parentElement
  let audio = div.getElementsByTagName('audio')[0]

  if (toggle === '0') {
    let src = audio.getAttribute('src')
    if (src === null) {
      fetch(`/media/${mediakey}`)
        .then(resp => resp.blob())
        .then(blob => {
          const url = window.URL.createObjectURL(blob)
          audio.setAttribute('src', url)
          audio.play()
          button.innerHTML = 'Pause'
          button.setAttribute('toggle', '1')
        })
    } else {
      audio.play()
      button.innerHTML = 'Pause'
      button.setAttribute('toggle', '1')
    }
  } else {
    audio.pause()
    button.innerHTML = 'Play'
    button.setAttribute('toggle', '0')
  }
}

window.updateBite = updateBite
window.deleteBite = deleteBite
window.loadBite = loadBite
//
//
// if (navigator.mediaDevices.getUserMedia) {
//
//   const constraints = {audio: true}
//
//   const recordOnClick = function (recorder, startElem) {
//     return () => {
//       if (recorder.state === "inactive") {
//         recorder.start()
//         startElem.innerText = "Stop"
//       } else if (recorder.state === "recording") {
//         recorder.stop()
//         startElem.innerText = "Start"
//       }
//     }
//   }
//
//   const onSuccess = function (stream) {
//     const recordElem = document.getElementById("record")
//     const form = document.getElementById("bite-form")
//     const recorder = new MediaRecorder(stream)
//
//     let chunks = []
//
//     recordElem.onclick = recordOnClick (recorder, recordElem)
//
//     // recorder.onstop = (evt) => {
//     //   const blob = new Blob(chunks,  {type: 'audio/mpeg-3'});
//     //   sendData(blob).then((resp) => {
//     //     console.log(resp.status)
//     //     chunks = []
//     //   })
//     // }
//
//     form.onsubmit = (evt) => {
//       evt.preventDefault()
//       const blob = new Blob(chunks,  {type: 'audio/mpeg-3'});
//       let formData = new FormData(form)
//       formData.append("buffer", blob)
//       console.log(formData)
//       return fetch("/bite", {
//         method: "POST",
//         body: formData
//       }).then((resp) => {
//         chunks = []
//         console.log(resp.status)
//       })
//     }
//
//     recorder.ondataavailable = (evt) => {
//      chunks.push(evt.data);
//     }
//   }
//
//   const onError = function (err) {
//     console.error(err)
//   }
//
//
//   navigator.mediaDevices.getUserMedia(constraints).then(
//     onSuccess, onError);
// } else {
//   console.error("Recording not available")
// }
