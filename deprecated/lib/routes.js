"use strict"

const path = require("path")

const home = require("./controllers/home.js")
const bite = require("./controllers/bite.js")
const user = require("./controllers/user.js")

module.exports = [
  {
    method: "POST",
    path: "/user",
    handler: user.create,
    config: {
      description: "Adds a new user",
      payload: {
        multipart: true,
      }
    }
  },
  {
    method: "GET",
    path: "/users",
    handler: user.list,
    config: {
      description: "List all users"
    }
  },
  {
    method: "GET",
    path: "/user/{id}",
    handler: user.get,
    config: {
      description: "Get a user by id"
    }
  },
  {
    method: "PUT",
    path: "/user/{id}",
    handler: user.update,
    config: {
      description: "Updates the selected user",
      payload: {
        multipart: true,
      }
    }
  },
  {
    method: "DELETE",
    path: "/user/{id}",
    handler: user.delete,
    config: {
      description: "Deletes the selected user"
    }
  },

  {
    method: "GET",
    path: "/bites",
    handler: bite.list,
    config: {
      description: "Gets all the bites available"
    }
  },
  {
    method: "POST",
    path: "/bite",
    handler: bite.create,
    config: {
      description: "Adds a new bite",
      payload: {
        multipart: true,
      }
    }
  },
  {
    method: "GET",
    path: "/bite/{id}",
    handler: bite.get,
    config: {
      description: "Gets the content of a bite"
    }
  },
  {
    method: "PUT",
    path: "/bite/{id}",
    handler: bite.update,
    config: {
      description: "Updates the selected bite",
      payload: {
        multipart: true,
      }
    }
  },
  {
    method: "DELETE",
    path: "/bite/{id}",
    handler: bite.delete,
    config: {
      description: "Deletes the selected bite"
    }
  },
  {
    method: "GET",
    path: "/",
    handler: home,
    config: {
      description: "Gets all the bites available"
    }
  },
  {
    // Static files
    method: "GET",
    path: "/{param*}",
    handler: {
      directory: {
        path: path.join(__dirname, "../static/public")
      }
    },
    config: {
      description: "Provides static resources"
    }
  }
]
