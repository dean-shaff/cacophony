"use strict"

jest.setTimeout(7000)

const fs = require("fs")
const path = require("path")

const { expect } = require('@hapi/code');
const { init } = require("./../../lib/server.js")
const models = require("./../../lib/models/")

const testFilePath = path.join(__dirname, "test.mp3")
const testFileKey = "test.mp3"

// const testFilePath = path.join(__dirname, "test.txt")
// const testFileKey = "test.txt"

describe("biteMedia", () => {

  let server

  beforeEach(async () => {
    server = await init();
  })

  afterEach(async () => {
    await server.stop();
  })

  // test("GET /bites", async () => {
  //   const res = await server.inject({
  //     method: "GET",
  //     url: "/bites"
  //   })
  //   expect(res.statusCode).to.equal(200);
  // })
  //
  // test("POST /media/", async () => {
  //   const stream = fs.readFileSync(testFilePath)
  //   // const stream = fs.createReadStream(testFilePath)
  //   const res = await server.inject({
  //     method: "POST",
  //     url: "/media/",
  //     payload: {
  //       buffer: stream,
  //       key: testFileKey
  //     }
  //   })
  //   expect(res.statusCode).to.equal(204)
  // })
  // test("GET /media/{id}", async () => {
  //   const result = await models.BiteMedia.findAll({
  //     order: [["date", "DESC"]]
  //   })
  //   console.log(result)
  // })
  //
  // test("PUT /bite/{id}", async () => {
  //
  // })
  //
  // test("DELETE /bite/{id}", async () => {
  //
  // })

})
