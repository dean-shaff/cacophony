import React from "react";
import ReactDOM from "react-dom";
import App from "./App.js";

import "./../../node_modules/bulma/css/bulma.css";

ReactDOM.render(<App bitesPerRow="4" />, document.getElementById("root"));
