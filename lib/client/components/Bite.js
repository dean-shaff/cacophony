import React, { Component } from "react"

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlay, faPause } from '@fortawesome/free-solid-svg-icons'

class Bite extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    let icon
    if (this.props.playing) {
      icon = <FontAwesomeIcon icon={faPause}/>
    } else {
      icon = <FontAwesomeIcon icon={faPlay}/>
    }
    return (
      <div>
        <h3 className="title is-3">{this.props.title}</h3>
        <h4 className="title is-4">{this.props.date}</h4>
        <button className="button" type="button">Update</button>
        <button className="button" type="button">Delete</button>
        <div>
          <button className="button" type="button">
            {icon}
          </button>
          <audio></audio>
        </div>
      </div>
    )
  }
}

export default Bite
