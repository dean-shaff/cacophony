import React, { Component } from "react"

import Bite from "./components/Bite.js"

import { getBites } from "./util.js"


class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      bites: null
    }
  }

  async componentDidMount() {
    let obj = await getBites()
    this.setState({
      ["bites"]: obj.bites
    })
  }

  renderBites() {
    const bitesPerRow = this.props.bitesPerRow
    const bites = this.state.bites
    if (bites) {
      let nBites = bites.length
      let rows = Math.ceil(nBites / bitesPerRow)
      let bitesGrid = []
      for (let irow=0; irow<rows; irow++) {
        let row = []
        for (let icol=0; icol<bitesPerRow; icol++) {
          let idx = icol + bitesPerRow*irow
          if (idx < nBites) {
            row.push(<div key={icol} className="tile is-child is-3 box"><Bite playing={false} {...bites[idx]}></Bite></div>)
          } else {
            break
          }
        }
        bitesGrid.push((
          <div key={irow} className="tile is-ancestor">
            <div className="tile is-parent">
              {row}
            </div>
          </div>
        ))
      }
      return <div className="container">{bitesGrid}</div>
    } else {
      return null
    }
  }

  render() {

    return (
      <div>
      <nav className="navbar is-spaced has-shadow" role="navigation" aria-label="main navigation">
        <div className="container">
          <div className="navbar-brand">
            <a className="navbar-item">
              <h1 className="title is-1">Cacophony</h1>
            </a>
          </div>
        </div>
        <div className="navbar-menu">
          <div className="navbar-end">
            <div className="navbar-item">
              <div className="buttons">
                <a className="button is-primary">Sign Up</a>
                <a className="button is-light">Login</a>
              </div>
            </div>
          </div>
        </div>
      </nav>
      {this.renderBites()}
      </div>
    )
  }
}

export default App
