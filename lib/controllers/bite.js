const fs = require("fs")
const path = require("path")

const uuid = require("uuid")
const slug = require("slug")

const Moment = require("moment");
const AWS = require("aws-sdk");

const settings = require("../../settings.js")
const mediaSettings = settings[settings.env].media
const { Bite } = require("./../models")


const accessKeyId = process.env["AWS_ACCESS_KEY_ID"]
const secretAccessKey = process.env["AWS_SECRET_KEY"]

if (accessKeyId === "" || secretAccessKey === "") {
  console.error("controllers/biteMedia.js: AWS tokens are not set")
}

const s3 = new AWS.S3({
  "accessKeyId": accessKeyId,
  "secretAccessKey": secretAccessKey
});

const bucketName = "cacophony"


const deleteMedia = async function (mediaKey) {
  console.log(`controllers.bite.deleteMedia: mediaKey=${mediaKey}`)
  if (mediaSettings.storage === "s3") {
    throw new Error("deleteMedia not implemented for s3")
  } else if (mediaSettings.storage === "local") {
    const mediaDir = mediaSettings.dir
    const filePath = path.join(mediaDir, mediaKey)
    fs.unlinkSync(filePath)
  }
}


const loadMedia = async function (mediaKey) {
  console.log(`controllers.bite.loadMedia: mediaKey=${mediaKey}`)

  if (mediaSettings.storage === "s3") {
    const params = {Bucket: bucketName, Key: mediaKey}
    return new Promise((resolve, reject) => {
      s3.getObject(params, (err, data) => {
        if (err) {
          reject(err)
        } else {
          resolve(data)
        }
      })
    })
  } else if (mediaSettings.storage === "local") {
    const mediaDir = mediaSettings.dir
    const filePath = path.join(mediaDir, mediaKey)
    return fs.readFileSync(filePath)
  }
}

const saveMedia = async function (mediaKey, mediaObject) {
  console.log(`controllers.bite.saveMedia: mediaKey=${mediaKey}`)
  if (mediaSettings.storage === "s3") {
    const params = {Bucket: bucketName, Key: mediaKey, Body: mediaObject};
    return new Promise((resolve, reject) => {
      s3.putObject(params, (err, data) => {
        if (err) {
          reject(err)
        } else {
          resolve(data)
        }
      })
    })
  } else if (mediaSettings.storage === "local") {
    const mediaDir = mediaSettings.dir
    const filePath = path.join(mediaDir, mediaKey)
    fs.writeFileSync(filePath, mediaObject)
    return
  }
}


module.exports = {
  list: async function (request, h) {
    const result = await Bite.findAll({
      order: [["date", "DESC"]]
    })
    return {
      bites: result
    }
  },
  create: async function (request, h) {
    const userId = request.payload.id
    const title = request.payload.title
    console.log(`controllers.bite.create: title=${title}, description=${request.payload.description}`)
    const id = uuid.v4()
    const titleSlug = slug(title, { lower: true })
    let buffer = request.payload.buffer
    if ("data" in buffer) {
      buffer = Buffer.from(buffer.data)
    }

    const mediakey = `${titleSlug}_${id}.mp3`
    await saveMedia(mediakey, buffer)

    const result = await Bite.create({
      "date": new Date(),
      "title": title,
      "slug": titleSlug,
      "description": request.payload.description,
      "mediakey": mediakey,
      "userId": userId,
      "id": id
    })

    return result
  },
  update: async function (request, h) {
    const values = {
      title: request.payload.title,
      description: request.payload.description,
    };

    const options = {
      where: {
        id: request.params.id
      }
    };

    await Bite.update(values, options);
    const result = await Bite.findOne(options);
    return result
  },
  get: async function (request, h) {
    const bite = await Bite.findOne({
      where: {
        id: request.params.id
      }
    })
    return bite
  },
  delete: async function (request, h) {
    const bite = await Bite.findOne({
      where: {
        id: request.params.id
      }
    })
    if (bite != null) {
      const mediaPath = bite.dataValues.mediakey
      deleteMedia(mediaPath)
      await Bite.destroy({
        where: {
          id: request.params.id
        }
      })
    }
    return ""
  },
  getMedia: async function (request, h) {
    let mediaKey = request.params.key
    console.log(`controllers.bite.getMedia: ${mediaKey}`)
    let buffer = await loadMedia(mediaKey)
    return buffer
    // return {
    //   'buffer': buffer
    // }
  }
}
