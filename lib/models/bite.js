"use strict";

const uuid = require("uuid")
const Moment = require("moment");

module.exports = (sequelize, DataTypes) => {
  const Bite = sequelize.define("Bite", {
    date: {
      type: DataTypes.DATE,
      get: function() {
        return Moment(this.getDataValue("date")).format("MMMM Do, YYYY");
      }
    },
    title: DataTypes.STRING,
    slug: DataTypes.STRING,
    description: DataTypes.STRING,
    mediakey: DataTypes.STRING, // this is the s3 key name, or the name of the local mp3 file to serve up
    userId: DataTypes.UUID,
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: uuid.v4()
    },
  });

  return Bite;
};
